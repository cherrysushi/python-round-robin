# python round robin
my own python implementation of round robin

## usage
set `n` the number of process needed<br>
set `quantum` the quantum duration
just launch the script with python 3 and watch the results

## disclaimer
I didn't test this on python 2

# author
cherrysushi (me)
