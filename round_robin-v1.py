
# Python3 program for implementation of  
# RR scheduling 
# by cherrysushi

import random

# number of process (int)
n = 10

# quantum duration (int)
quantum = 1

def create_list(n):
    # creates list of n process with random duration
    proc = []

    for p in range(1,n+1):
        # random duration
        duration = random.randint(1, n+1)
        # adds 
        proc.append([p,duration])
    return proc

proc_list = create_list(n)

iter = 0
duration = 0

print(str(iter) + ": " + str(proc_list) + " " + str(duration) + " ms, " + str(len(proc_list)) + " process(es)")

# moves proc from first position to last position and removes quantum duration
while proc_list:
    
    # set 1st proc
    first = proc_list[0]

    # removes 1st proc from list
    proc_list.pop(0)
    
    # if duration left is smaller than quantum, store that, else store quantum
    if (first[1] < quantum):
        duration += first[1]
    else:
        duration += quantum

    # puts former 1st proc at the end of the list and removes quantum duration from proc duration only if proc duration > 0
    if (first[1] - quantum) > 0:

        # removes quantum duration from proc duration
        first[1] = first[1] - quantum
        # puts former 1st proc at the end of the list
        proc_list.append(first)
    
    iter += 1
    
    print(str(iter) + ": " + str(proc_list) + " " + str(duration) + " ms, " + str(len(proc_list)) + " process(es)")
print(str(duration/n) + " ms by process on average")
